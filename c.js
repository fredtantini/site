///////////////////////////////////////////////////////
// From http://diveintohtml5.info/examples/canvas-halma.html

var knightText = "♘";
var firstSquare;
var lastSquare;
var kBoardWidth = 8;
var kBoardHeight= 8;
var kSquareWidth = 25;
var kSquareHeight= 25;
var kPixelWidth = 1 + (kBoardWidth * kSquareWidth);
var kPixelHeight= 1 + (kBoardHeight * kSquareHeight);

var gCanvasElement;
var gDrawingContext;
var gStartElem;
var gEndElem;

var gSquares;
var gNumSquares = kBoardWidth * kBoardHeight;
var gMoveCount;
var gMoveCountElem;

function rgb(color){
    return "rgb("+color+","+color+","+color+")";
}

function colRowFromAlgebraic(coordAlg){
    //coord = "e2"
    try{
        var col = coordAlg[0].toUpperCase().charCodeAt()-65; //-"A"
        var row = kBoardHeight - parseInt(coordAlg[1]);
        if((col<0)||(col>=kBoardWidth)||(row<0)||(col>=kBoardHeight)){
            return;
        }
        return col+row*kBoardWidth;
    }
    catch(e){
        return;
    }
    return;
}

function algebraicFromPos(pos){
    //coord = "e2"
    var col = pos % (kBoardWidth);
    var row = Math.floor(pos/ kBoardWidth);
    return String.fromCharCode(col+65)+(kBoardHeight-row);
}


function getCursorPosition(e) {
    /* returns [row, column] */
    var x;
    var y;
    if (e.pageX != undefined && e.pageY != undefined) {
	x = e.pageX;
	y = e.pageY;
    }
    else {
	x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
	y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= gCanvasElement.offsetLeft;
    y -= gCanvasElement.offsetTop;
    x = Math.min(x, kBoardWidth * kSquareWidth);
    y = Math.min(y, kBoardHeight * kSquareHeight);
    
    return [Math.floor(x/kSquareWidth), Math.floor(y/kSquareHeight)];
}

function knightOnClick(e) {
    var coords = getCursorPosition(e);
    var coord1d = coords[0]+coords[1]*kBoardWidth
    if (gSquares.indexOf(coord1d)==-1){
        clickOnEmptyCell(coord1d, coords);
    }
    return;
}

function clickOnEmptyCell(coord1d, coords) {
    var whereAmI = gSquares[gSquares.length - 1];
    var x = whereAmI % (kBoardWidth);
    var y = Math.floor(whereAmI / kBoardWidth);
    var diff_x = Math.abs(coords[0]-x);
    var diff_y = Math.abs(coords[1]-y);
    if (((diff_x==1) && (diff_y==2)) ||
        ((diff_x==2) && (diff_y==1))){
        drawSquare(x, y, gMoveCount);
        gMoveCount++;
        gMoveCountElem.innerHTML = gMoveCount;

        gSquares.push(coord1d);
        drawSquare(coords[0],coords[1], knightText);
        if (gMoveCount==gNumSquares){
            drawSquare(1,1,"1");
        }
    }

    return;
}


function isTheGameOver() {
    return (gMoveCount == 64);
}

function drawBoard() {

    gDrawingContext.clearRect(0, 0, kPixelWidth, kPixelHeight);
    gDrawingContext.beginPath();
    
    gMoveCountElem.innerHTML = gMoveCount;

    for (var i = 0; i < kBoardWidth; i++) {
        for (var j = 0; j < kBoardHeight; j++) {
            drawSquare(i, j, "");
        }
    }

}

function drawSquare(column, row, txt) {
    var x = (column * kSquareWidth) + (kSquareWidth/2);
    var y = (row * kSquareHeight) + (kSquareHeight/2);
    gDrawingContext.fillStyle = ((column+row)%2)?rgb(50):rgb(200);
    gDrawingContext.fillRect(column*kSquareWidth, row*kSquareHeight, kSquareWidth, kSquareHeight);
    gDrawingContext.font = "bold 14px sans-serif";
    gDrawingContext.textBaseline = "middle";
    gDrawingContext.textAlign = "center";
    gDrawingContext.fillStyle = rgb(125);
    gDrawingContext.fillText(txt, x, y);
    
}


function newGame(){       
    //random between 1 and gNumSquares, and different
    firstSquare = Math.floor((Math.random() * (gNumSquares-1)) + 1);
    lastSquare = Math.floor((Math.random() * (gNumSquares-1)) + 1);
    while (lastSquare == firstSquare){
        lastSquare = Math.floor((Math.random() * (gNumSquares-1)) + 1);
    }
    
    resetGame();
}

function newBoard(){
    var startValue = gStartElem.value;
    var endValue = gEndElem.value;
    if (startValue == endValue){
        return;
    }
    firstSquare = colRowFromAlgebraic(startValue);
    lastSquare = colRowFromAlgebraic(endValue);
    
    resetGame();
}

function resetGame(){
    gStartElem.value = algebraicFromPos(firstSquare);
    gEndElem.value = algebraicFromPos(lastSquare);
    gSquares = [];
    gSquares.push(firstSquare);
    gMoveCount = 1;
    drawBoard();
    drawSquare((firstSquare % (kBoardWidth)), Math.floor(firstSquare / kBoardWidth), knightText);
    drawSquare(lastSquare % kBoardHeight, parseInt(lastSquare / kBoardWidth), gNumSquares);
}


function initGame(canvasElement, moveCountElement) {
    if (!canvasElement) {
        canvasElement = document.createElement("canvas");
	canvasElement.id = "knight_canvas";
	document.body.appendChild(canvasElement);
    }
    
    gCanvasElement = canvasElement;
    gCanvasElement.width = kPixelWidth;
    gCanvasElement.height = kPixelHeight;
    gCanvasElement.addEventListener("click", knightOnClick, false);
    gMoveCountElem = moveCountElement;
    gStartElem = document.getElementById('startElem');
    gEndElem = document.getElementById('endElem');
    gDrawingContext = gCanvasElement.getContext("2d");
 
    newGame();
}
