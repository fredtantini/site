
// https://github.com/ccampbell/mousetrap
// http://craig.is/killing/mice

Mousetrap.bind('alt+right', function(e) {
    org_demote();
});

Mousetrap.bind('alt+left', function(e) {
    org_promote();
});


Mousetrap.bind('control+c control+c', function(e) {
    org_set_tags();
});


Mousetrap.bind('alt+shift+w', function(e) {
    test();
});


//todo
Mousetrap.bind('alt+shift+right', function(e) {
    org_demote_subtree();
});

// pour un elem sur une des deux lignes (headline/content), retourne le tr headline
function get_tr_headline(elem_in_table){
    var elem_parent = elem_in_table;
    //on remonte tant qu'on peut
    while(elem_parent.tagName.toLowerCase() != "tr" && elem_parent.tagName.toLowerCase() != "body"){
        elem_parent = elem_parent.parentNode;
    }
    //si on est trop remonté, on n'était pas dans une ligne de la table!!!
    if (elem_parent.tagName.toLowerCase() == "body"){
        throw "not in table";
    }
    var elem_tr = elem_parent;
    if (elem_parent.className == "content"){
        // il faut prendre le tr precedent
        return elem_tr.parentNode.rows[elem_tr.rowIndex -1];
    }else{
        return elem_tr;
    }
}

// pour un tr headline, retourne le niveau (un entier)
function get_level_tr_headline(elem_tr_headline){
    try{
        return elem_tr_headline.getElementsByClassName("level")[0].innerHTML.trim().length;
    } catch (e) {
        console.log("pas dans un tr headline ? :" + e);
    }
}

function make_string(char_c, int_nb){
    var ret="";
    try{
        for (var j=1;j<=int_nb;j++){
            ret = ret + char_c;
        }
        return ret;
    }
    catch(e){
        console.log("make_string :" + e);
    }
}

// pour un tr headline, maj le niveau
function set_level(elem_tr_headline, i_lvl){
    try{
        var new_level = make_string("*", i_lvl);
        elem_tr_headline.getElementsByClassName("level")[0].innerHTML = new_level;
        return 1;
    } catch (e) {
        console.log("pas dans un tr ? pas un entier ? " + e);
        return -1;
    }
}


//single space after period
//http://stackoverflow.com/questions/8867751/how-to-get-the-row-index-and-send-it-as-a-parameter-to-javascript-function
//moveRow(index, destination)
//insertRow(index) Adding the row inserts an empty element, to which you add cells via the insertCell( ) method.
//insertCell(index) Inserts a td element nested within the current tr element.
//http://stackoverflow.com/questions/9428026/add-div-dynamically-javascript

//retourne un index, un tr_healdline en arg !
function index_next_tr_same_level(elem_tr){
    try{
        var index_tr = elem_tr.rowIndex + 2;
        var level_to_find = get_level_tr_headline(elem_tr);
        var level_of_next_tr = 0;
        var elem_rows = elem_tr.parentNode.rows;
        var len_table = elem_rows.length;
        var found = false;
        var exit = false
        while (index_tr < len_table && !exit){
            level_of_next_tr = get_level_tr_headline(elem_rows[index_tr]);
            if (level_of_next_tr <= level_to_find){
                exit = true;
                if (level_to_find == level_of_next_tr){
                    found = true;
                }
            }
            index_tr = index_tr + 2;
        }
        if(found){
            return index_tr - 2;
        }else{
            return elem_tr.rowIndex;
        }
    }catch(e){
        consol.log("pas un tr ? " +e);
    }
}

//
function org_demote_subtree_from_trhl(elem_tr){
    try{
        var index_tr = elem_tr.rowIndex + 2;
        var level_to_find = get_level_tr_headline(elem_tr);
        var level_of_next_tr = 0;
        var elem_rows = elem_tr.parentNode.rows;
        org_demote_elem(elem_rows[index_tr-2]);
        var len_table = elem_rows.length;
        var exit = false
        while (index_tr < len_table && !exit){
            level_of_next_tr = get_level_tr_headline(elem_rows[index_tr]);
            if (level_of_next_tr > level_to_find){
                org_demote_elem(elem_rows[index_tr]);
            }else{
                exit = true;
            }
            index_tr = index_tr + 2;
        }
    }catch(e){
        consol.log("org_demote_subtree_from_trhl " +e);
    }
}


function org_demote_subtree_elem(elem_activ){
    try{
        var elem_tr_hl = get_tr_headline(elem_activ);
        org_demote_subtree_from_trhl(elem_tr_hl);
    }catch(e){
        console.log("org_demote_subtree_elem " + e);
    }
}

function org_demote_subtree(){
    org_demote_subtree_elem(document.activeElement);
}

//augmente d'un niveau
function org_demote_tr(elem_tr_hl){
    try{
        var int_lvl = get_level_tr_headline(elem_tr_hl);
        set_level(elem_tr_hl, int_lvl + 1);
    }catch(e){
        console.log("org_demote_tr " + e);
    }
}

//augmente d'un niveau
function org_demote_elem(elem_activ){
    try{
        var elem_tr_hl = get_tr_headline(elem_activ);
        org_demote_tr(elem_tr_hl);
    }catch(e){
        console.log("org_demote_elem " + e);
    }
}

function org_demote(){
    org_demote_elem(document.activeElement);
}

//baisse d'un niveau
function org_promote_elem(elem_activ){
    try{
        var elem_tr_hl = get_tr_headline(elem_activ);
        var int_lvl = get_level_tr_headline(elem_tr_hl);
        set_level(elem_tr_hl, Math.max(1,int_lvl - 1));
    }catch(e){
        console.log("org_promote " + e);
    }
}

function org_promote(){
    org_promote_elem(document.activeElement);
}


function org_set_tags(){
    var elem_parent = document.activeElement.parentNode;
    if (elem_parent.className=="content"){
        //on remonte d'un tr
        
    }else{
        //on est sur la bonne ligne, on remonte jusqu'au tr
    }
}

//function org_ set priority
// Cc Cc:tag
//CcCt : todo/done/
// circle visibility tab/Stab
// sauve Cx s
// nouvelle liste Cx f/b
// C-Enter : nouvel item en fin d'arbre si a,aa,ab,c,ca, sur a -> b
// M-Enter : nouvel item mm niveau, on s'insère si a,aa,ab,c,ca, sur a -> a'
//fin todo



function test(){
    var a = document.activeElement;
    var p = get_tr_headline(a);
    console.log(p.rowIndex);
    console.log(index_next_tr_same_level(p));
}


var etest = document.all[31];