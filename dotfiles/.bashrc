debug=0

# Source global definitions
if [ -f /etc/bash.bashrc ]; then
    . /etc/bash.bashrc
fi

#execute 2 versions of firefox/iceweasel
export MOZ_NO_REMOTE=1
export HISTFILE=${HOME}/.bash.d/history
export HISTCONTROL=ignoreboth
export HISTIGNORE="&:ls:[bf]g:exit:llrt:ll[la]:cd:"
shopt -s histappend
shopt -s cdspell
shopt -s cmdhist

# Source les variables d'environnement
if [ -f ${HOME}/.bash.d/env ]; then
    . ${HOME}/.bash.d/env
fi

# Source le fichier definissant le prompt
if [ -f ${HOME}/.bash.d/prompt ]; then
    . ${HOME}/.bash.d/prompt
fi

if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi


# Source le fichier definissant les aliases
if [ -f ${HOME}/.bash.d/aliases ]; then
    . ${HOME}/.bash.d/aliases
fi

# Source les completions perso
if [ -f ${HOME}/.bash.d/completion ]; then
    . ${HOME}/.bash.d/completion
fi

unset debug
ulimit -c 0
