;---------------------------------------------------------------------
;.emacs file
;---------------------------------------------------------------------
(setq max-specpdl-size 1000)

;---------------------------------------------------------------------
;Permet la surbrillance de la region selectionnee a la souris
;---------------------------------------------------------------------
(transient-mark-mode t)

;---------------------------------------------------------------------
;Permet la surbrillance des parenth�ses par paires
;---------------------------------------------------------------------
(load-library "paren")
(show-paren-mode t)

;---------------------------------------------------------------------
;Pour les fichier gzip et/ou tar
;---------------------------------------------------------------------
(load-library "jka-compr")
(auto-compression-mode t)

;---------------------------------------------------------------------
;Ne va pas a la ligne par defaut
;---------------------------------------------------------------------
;(setq default-truncate-lines t)

;---------------------------------------------------------------------
;Replace sans conversion de casse
;---------------------------------------------------------------------
(setq-default case-replace nil)

;---------------------------------------------------------------------
;Accents
;---------------------------------------------------------------------
;; Display european characters in text-mode
; Emacs 21
; (standard-display-european t)
; Emacs 22
(set-language-environment 'Latin-1)

;---------------------------------------------------------------------
;Variables d'environnement et definition de ma bibliotheque
;---------------------------------------------------------------------
(defvar home-dir (getenv "HOME" ))
(setenv "TEXINPUTS" (concat ".:./images//:" home-dir "/Config/Latex//:"))
(setenv "BIBINPUTS" (concat ".:./bib//:" home-dir "/tex/bib//:"))
(setenv "BSTINPUTS" (concat ".:./bib//:" home-dir "/tex/bib/:"))
(setenv "PATH" (concat ".:" home-dir "/bin:" (getenv "PATH")))
(defvar home-lisp-dir (list (concat home-dir "/.emacs.d")))
(if home-lisp-dir
    (setq load-path (append home-lisp-dir load-path)))

;---------------------------------------------------------------------
;Pour ne pas lire la configuration par d�faut
;---------------------------------------------------------------------
(setq inhibit-default-init t)
(setq inhibit-startup-message t)
;---------------------------------------------------------------------
;Shell par d�faut
;---------------------------------------------------------------------
(setq sh-shell-file '"/bin/bash")

;---------------------------------------------------------------------
;Indiquer le numero de la ligne et de la colonne
;---------------------------------------------------------------------
(line-number-mode t)
(column-number-mode t)

;---------------------------------------------------------------------
;Menu buffer amelior�
;---------------------------------------------------------------------
(require 'msb)
;(setq msb-menu-cond msb--many-menus)
(setq msb-display-most-recently-used 5)
(setq msb-files-by-directory t)
(msb-mode 1)


;---------------------------------------------------------------------
;Font-lock
;---------------------------------------------------------------------
(if (and (boundp 'window-system) window-system)
    (require 'font-lock))

;---------------------------------------------------------------------
;Apparance
;---------------------------------------------------------------------
(set-foreground-color "white");wheat
(set-background-color "black")
(set-cursor-color "orange")
(set-mouse-color "GreenYellow")
(set-default-font "-adobe-courier-bold-r-normal--14-100-100-100-m-90-iso8859-1")
(setq default-frame-alist
      '((minibuffer . t)
	(cursor-color . "orange")
	(menu-bar-lines . 3)
	(mouse-color . "GreenYellow")
	(foreground-color . "white");wheat
	(background-color  . "black")
	(font . "-adobe-courier-bold-r-normal--14-100-100-100-m-90-iso8859-1")))

;Pour les nouvelles versions d'emacs 21...
(if (string-equal "21" (substring (emacs-version) 10 12))
    (progn
      ;;ne pas faire clignoter le cursor
      (blink-cursor-mode nil)
      ;; Turn on menu bar (this bar has text)
      (menu-bar-mode 1)
      ;; Turn off tool bar (this bar has icons)
      (tool-bar-mode 0)
      ;; Turn off tooltip mode for tool bar
      (tooltip-mode 0)))

;---------------------------------------------------------------------
;FRAMEPOP
;display some particular buffer (as help buffers) in another frame
;---------------------------------------------------------------------
(cond
 (window-system
  (require 'advice)
  (require 'framepop)
  (define-key global-map [f2] framepop-map)
  (setq framepop-frame-parameters
	'(
	  (name . nil)
	  (unsplittable . t)
	  (minibuffer . nil)
	  (menu-bar-lines . 0)
	  (left . -1)
	  (top . 100)
	  (cursor-color . "Orchid")
	  (mouse-color . "ForestGreen")
	  (foreground-color . "black")
	  (background-color  . "wheat")
	  (font . "-adobe-courier-bold-r-normal--12-120-75-75-m-70-iso8859-1")))

  (setq special-display-function 'framepop-special-display)
  (setq special-display-buffer-names
	'("*Shell Command Output*" "*TeX Help*" "*TeX background*" "*infos*" "*compilation*" "*Completions*"))
  (setq special-display-regexps
	'("\\*.*output\\*" ))

  (setq framepop-max-frame-size 50)
  (setq framepop-lines
	(function
	 (lambda (buf)
	   (if (string= (buffer-name buf) "*Buffer List*") nil
	     (save-excursion
	       (set-buffer buf)
	       (if mode-line-process
		   framepop-max-frame-size
		 (+ (count-lines (point-min) (point-max)) 5)))))))))


;---------------------------------------------------------------------
;SPEEDBAR
;quick access to files and tags
;---------------------------------------------------------------------
;(autoload 'speedbar-frame-mode "speedbar" "Popup a speedbar frame" t)
;(autoload 'speedbar-get-focus "speedbar" "Jump to speedbar frame" t)
;(define-key-after (lookup-key global-map [menu-bar tools])
;  [speedbar] '("Speedbar" . speedbar-frame-mode) [calendar])
;(setq speedbar-frame-parameters
;      '(
;	(name . nil)
;	(unsplittable . t)
;	(menu-bar-lines . 0)
;	(minibuffer . nil)
;	(width . 17)
;	(height . 66)
;	(left . +1087)
;	(top . +0)))
;(setq speedbar-show-unknown-files t)

;---------------------------------------------------------------------
;Fonctions perso
;---------------------------------------------------------------------
(load-library "my-functions")

;---------------------------------------------------------------------
;Default Mode
;---------------------------------------------------------------------
(defun my-default-mode ()
  (interactive))

;---------------------------------------------------------------------
;Text Mode
;---------------------------------------------------------------------
(defun my-text-mode-hook ()
  (my-default-mode))

;---------------------------------------------------------------------
;Python Mode
;---------------------------------------------------------------------
 (autoload 'python-mode "python-mode" "Python Mode." t)
 (add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
 (add-to-list 'interpreter-mode-alist '("python" . python-mode))

 (add-hook 'python-mode-hook
       	  '(lambda ()
             (load-library "py-mode-ext")  
             (load-library "pyp")
             (require 'pycomplete)
             (define-key py-mode-map [f3] 'pyp)
             (define-key py-mode-map "\C-c\C-c" 'py-execute-prog)
             (define-key py-mode-map "\C-c\C-g" 'py-call-pdb)
             (define-key py-mode-map "\C-c\C-w" 'pychecker))
             (set (make-variable-buffer-local 'beginning-of-defun-function)
                  'py-beginning-of-def-or-class)
             (setq outline-regexp "def\\|class "))

;---------------------------------------------------------------------
;Language C
;---------------------------------------------------------------------
(defun my-c-mode-hook()
  (my-default-mode)
  (setq c-auto-newline nil)  ; Non-nil means automatically newline before and after
			     ; braces, and after colons and semicolons, inserted in C
			     ; code.
  (setq c-tab-always-indent t) ; Non-nil means TAB in C mode should always reindent the
			       ; current line.
  (set-fill-column 85)
  ;;(setq search-upper-case t) ; faire la diff�rence maj-min dans les recherches.

  ;liaison des touches aux fonctions
  (define-key c-mode-map "\C-c\C-c" 'compile)
)



;---------------------------------------------------------------------
;JAVA
;---------------------------------------------------------------------
;(defun my-java-mode-hook ()
;  (my-default-mode)
;  (cond (window-system
;	 (require 'andersl-java-font-lock)
;	 (turn-on-font-lock))))
;(require 'jde)
(setq semanticdb-default-save-directory (expand-file-name "~/.emacs.d/semantic.cache/"))

;---------------------------------------------------------------------
;CAML
;---------------------------------------------------------------------
(defun my-tuareg-mode-hook()
  (my-default-mode)
  (tuareg-lazy-= t)
  (tuareg-lazy-paren t)
  (tuareg-in-indent 0)
  (tuareg-font-lock-function-name '("brown" "cyan" nil t t t))
  (tuareg-font-lock-comment '("pink4" "moccasin" nil t nil t))
  (auto-fill-mode 1)
  (if (featurep 'sym-lock)
      (setq sym-lock-mouse-face-enabled nil)))

;---------------------------------------------------------------------
;TeX
;---------------------------------------------------------------------
(defun my-tex-mode-hook ()
  (my-default-mode)
  (real-tex-hook tex-mode-map)
  (setq tab-width 4))

;---------------------------------------------------------------------
;LaTeX
;---------------------------------------------------------------------
(defun my-LaTeX-mode-hook ()
  (my-default-mode)
  ;(require 'TeX-advanced-mode)
  ;(real-tex-hook tex-mode-map)
  ;(real-tex-hook (current-local-map))

  (setq-default TeX-auto-regexp-list 'LaTeX-auto-minimal-regexp-list)
  (define-key LaTeX-mode-map "\e\C-q" 'LaTeX-fill-region)
  (define-key LaTeX-mode-map "\C-c<" 'latex-insert-guillleft)
  (define-key LaTeX-mode-map "\C-c>" 'latex-insert-guillright)
  (setq tab-width 4)
  ;;(set-fill-column (- (frame-width) 5))
  (set-fill-column 80)
  (auto-fill-mode 1)
  ;(hilit-install-line-hooks)
)

;---------------------------------------------------------------------
;AucTeX
;---------------------------------------------------------------------
(require 'tex-site)
(setq-default enable-local-eval t)
(setq-default TeX-master nil)

;(autoload 'ltxtab-init "latextab" "" t)
;(add-hook 'LaTeX-mode-hook 'ltxtab-init t)
;(setq ltxtab-override-auctex nil)

;---------------------------------------------------------------------
;CSS
;---------------------------------------------------------------------
(autoload 'css-mode "css-mode")
(setq auto-mode-alist
      (cons '("\\.css\\'" . css-mode) auto-mode-alist))
(setq cssm-indent-function #'cssm-c-style-indenter)


;---------------------------------------------------------------------
;Quitte sur USR1
;---------------------------------------------------------------------
(defun my-usr1-handling ()
  (interactive)
  (save-buffers-kill-emacs t))
(global-set-key [usr1-signal] 'my-usr1-handling)


;---------------------------------------------------------------------
;HOOKS
;---------------------------------------------------------------------
(add-hook 'text-mode-hook        'my-text-mode-hook)
(add-hook 'tex-mode-hook         'my-tex-mode-hook)
(add-hook 'LaTeX-mode-hook       'my-LaTeX-mode-hook)
(add-hook 'c-mode-hook           'my-c-mode-hook)
;(add-hook 'java-mode-hook        'my-java-mode-hook)
(add-hook 'tuareg-mode-hook      'my-tuareg-mode-hook)

;---------------------------------------------------------------------
;Extensions de fichiers
;---------------------------------------------------------------------
;(autoload 'real-tex-hook "TeX-advanced-mode" "addTexMode")
;(autoload 'javascript-mode "javascript-cust" "javascript mode" t nil)
(autoload 'tuareg-mode "tuareg" "Major mode for editing Caml code" t)
(autoload 'camldebug "camldebug" "Run the Caml debugger" t)
;;(autoload 'php-mode "php-mode" "PHP editing mode" t)
;(add-to-list 'auto-mode-alist '("\\.js$" . javascript-mode))
(add-to-list 'auto-mode-alist '("\\.ml\\w?" . tuareg-mode))
(add-to-list 'auto-mode-alist '("\\.tex$" . latex-mode))
;;(add-to-list 'auto-mode-alist '("\\.php3$" . php-mode))
;;(add-to-list 'auto-mode-alist '("\\.php$" . php-mode))
;;(add-to-list 'auto-mode-alist '("\\.inc$" . php-mode))
(add-to-list 'auto-mode-alist '("\\.gnuplot$" . gnuplot-mode))

;---------------------------------------------------------------------
;Souris avec molette
;---------------------------------------------------------------------
(defun up-slightly () (interactive) (scroll-up 5))
(defun down-slightly () (interactive) (scroll-down 5))
(global-set-key [mouse-4] 'down-slightly)
(global-set-key [mouse-5] 'up-slightly)
(defun up-one () (interactive) (scroll-up 1))
(defun down-one () (interactive) (scroll-down 1))
(global-set-key [S-mouse-4] 'down-one)
(global-set-key [S-mouse-5] 'up-one)
(defun up-a-lot () (interactive) (scroll-up))
(defun down-a-lot () (interactive) (scroll-down))
(global-set-key [C-mouse-4] 'down-a-lot)
(global-set-key [C-mouse-5] 'up-a-lot)

;---------------------------------------------------------------------
;Permet de sauvegarder l'emplacement ou on est avant de fermer le fichier
;---------------------------------------------------------------------
(require 'saveplace)
(setq-default save-place t)

;---------------------------------------------------------------------
;Pour les couleurs dans les shells
;---------------------------------------------------------------------
(autoload 'ansi-color-apply "ansi-color"
  "Translates ANSI color escape sequences into text-properties." t)

;---------------------------------------------------------------------
;Evite les beep
;---------------------------------------------------------------------
(setq visible-bell t)

;---------------------------------------------------------------------
;Retrouver les buffers de la fois pr�c�dente
;---------------------------------------------------------------------
;;;saving session
(desktop-save-mode 1)

;; Customization follows below
;; Some var
(setq history-length 250)

(setq desktop-globals-to-save
      (append
       '(desktop-missing-file-warning
	 tags-file-name
	 tags-table-list
	 search-ring
	 regexp-search-ring
	 register-alist)
       '((read-command-history . 9)
	 (file-name-history . 9)
	 (command-history . 9)
	 (grep-history . 9)
	 (compile-history . 9)
	 (minibuffer-history . 9)
	 (gud-perldb-history . 9)
	 (hypropos-regexp-history . 9))
       '(desktop-globals-to-save)))

(add-hook 'kill-emacs-hook
	  '(lambda ()
	     (desktop-truncate search-ring 3)
	     (desktop-truncate regexp-search-ring 3)))

;;not to save
(setq desktop-buffers-not-to-save
        (concat "\\("
                "^nn\\.a[0-9]+\\|\\.log\\|(ftp)\\|^tags\\|^TAGS"
                "\\|\\.emacs.*\\|\\.diary\\|\\.newsrc-dribble\\|\\.bbdb"
	        "\\)$"))
(add-to-list 'desktop-modes-not-to-save 'dired-mode)
(add-to-list 'desktop-modes-not-to-save 'Info-mode)
(add-to-list 'desktop-modes-not-to-save 'info-lookup-mode)
(add-to-list 'desktop-modes-not-to-save 'fundamental-mode)

(setq desktop-save 'if-exists)


;;(desktop-read)
;--------------------------------------------------------
;D�finitions des touches globales
;---------------------------------------------------------------------
(global-set-key [deleteline] 'effacer-ligne)
(global-set-key [insertline] 'inserer-ligne)
(global-set-key [C-return] 'find-file-at-point)
(global-set-key [C-tab] 'other-frame)
(global-set-key [M-tab] 'dabbrev-expand) ;; completion
(global-set-key "\C-xl" 'goto-line)
;(global-set-key [home] 'beginning-of-buffer)
;(global-set-key [end] 'end-of-buffer)
;(global-set-key [f11] 'beginning-of-buffer)
;(global-set-key [f12] 'end-of-buffer)
(global-set-key (kbd "<delete>") 'delete-char)

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(TeX-command-Show "View PS")
 '(TeX-command-list (quote (("TeX" "tex '\\nonstopmode\\input %t'" TeX-run-TeX nil t) ("TeX Interactive" "tex %t" TeX-run-interactive nil t) ("LaTeX" "%l '\\nonstopmode\\input{%t}'" TeX-run-LaTeX nil "nil") ("LaTeX Interactive" "%l %t" TeX-run-interactive nil t) ("LaTeX2e" "latex2e '\\nonstopmode\\input{%t}'" TeX-run-LaTeX nil t) ("View PS" "gv --noresize %f" TeX-run-silent nil "nil") ("View" "%V " TeX-run-silent nil nil) ("Print" "%p %r " TeX-run-command t nil) ("Queue" "%q" TeX-run-background nil nil) ("File" "%(o?)dvips %d -o %f" TeX-run-command nil nil) ("BibTeX" "bibtex %s" TeX-run-BibTeX nil nil) ("Index" "makeindex %s" TeX-run-command nil t) ("Check" "lacheck %s" TeX-run-compile nil t) ("Spell" "<ignored>" TeX-run-ispell-on-document nil nil) ("Other" "" TeX-run-command t t) ("Makeinfo" "makeinfo %t" TeX-run-compile nil t) ("Makeinfo HTML" "makeinfo --html %t" TeX-run-compile nil t) ("AmSTeX" "amstex '\\nonstopmode\\input %t'" TeX-run-TeX nil t))))
 '(global-font-lock-mode t nil (font-lock))
 '(org-agenda-files (quote ("~/Donnees/Org/first.org" "~/Donnees/Org/exemple.org")))
 '(speedbar-directory-unshown-regexp "^\\(CVS\\|RCS\\|\\.\\|SCCS\\)"))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(font-lock-comment-face ((t (:bold t :italic t :foreground "CadetBlue"))))
 '(font-lock-constant-face ((((class color) (background dark)) (:foreground "Khaki"))))
 '(font-lock-crossref-face ((t (:foreground "chocolate"))))
 '(font-lock-decl-face ((t (:bold t :foreground "Turquoise"))))
 '(font-lock-define-face ((t (:bold t :foreground "pink"))))
 '(font-lock-defun-face ((t (:bold t :foreground "Cyan"))))
 '(font-lock-directory-face ((t (:foreground "limegreen"))))
 '(font-lock-error-face ((t (:foreground "green"))))
 '(font-lock-formula-face ((t (:foreground "magenta"))))
 '(font-lock-function-name-face ((t (:bold t :foreground "Turquoise"))))
 '(font-lock-include-face ((t (:bold t :foreground "pink"))))
 '(font-lock-input-face ((t (:foreground "Pink"))))
 '(font-lock-keyword-face ((t (:bold t :foreground "Cyan"))))
 '(font-lock-label-face ((t (:foreground "orange"))))
 '(font-lock-link-face ((t (:bold t :foreground "Turquoise"))))
 '(font-lock-string-face ((t (:foreground "LightGray"))))
 '(font-lock-type-face ((t (:bold t :foreground "Yellow"))))
 '(font-lock-variable-name-face ((t (:foreground "MediumSeaGreen")))))

(put 'eval-expression 'disabled nil)
(put 'narrow-to-region 'disabled nil)
(put 'narrow-to-page 'disabled nil)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)

;---------------------------------------------------------------------
;Lance la speedbar au demarage
;---------------------------------------------------------------------
;(speedbar)

;---------------------------------------------------------------------
;Lance le serveur d'�dition
;---------------------------------------------------------------------
(server-start)

; ---- language-env DON'T MODIFY THIS LINE!
(if (>= emacs-major-version 21)
    (progn
      (set-language-environment "Latin-9")
      (setq selection-coding-system 'compound-text-with-extensions)
      )
  (if (>= emacs-major-version 20)
      (set-language-environment "Latin-1")
    (require 'iso-syntax)))

;; Names for calendar command.
;; These should be derived from nl_langinfo() by emacs
;;
(defvar calendar-day-name-array
  ["lun" "mar" "mer" "jeu" "ven" "sam" "dim"])
(defvar calendar-month-name-array
  ["janvier" "f�vrier" "mars" "avril" "mai" "juin" 
   "juillet" "ao�t" "septembre" "octobre" "novembre" "d�cembre"])

; ---- language-env end DON'T MODIFY THIS LINE!

;(setq locale-coding-system 'iso-8859-15)
;(set-terminal-coding-system 'iso-8859-15)
;(set-keyboard-coding-system 'iso-8859-15)
(set-selection-coding-system 'iso-8859-15)
;(prefer-coding-system 'iso-8859-15)


;;;;;;;;;;;;;;;;;;;;
;; set up unicode
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
;; This from a japanese individual. I hope it works.
(setq default-buffer-file-coding-system 'utf-8)
;; From Emacs wiki
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))
;
;
;
;(setq x-select-enable-clipboard t)
;(setq interprogram-paste-function 'x-cut-buffer-or-selection-value)

;(global-set-key [(control x)(left)] 'ignore)
;(global-set-key [(control x)(left)] 'next-buffer)
;(global-set-key [(control x)(right)] 'ignore)
;(global-set-key [(control x)(right)] 'previous-buffer)
(require 'swbuff)
(global-set-key (kbd "C-x <right>") 'swbuff-switch-to-next-buffer)
;C-x b
(global-set-key (kbd "C-x <left>") 'swbuff-switch-to-previous-buffer)


;(iswitchb-mode 1)
;(defun iswitchb-local-keys ()
;  (mapc (lambda (K) 
;	  (let* ((key (car K)) (fun (cdr K)))
;	    (define-key iswitchb-mode-map (edmacro-parse-keys key) fun)))
;	'(("<right>" . iswitchb-next-match)
;	  ("<left>"  . iswitchb-prev-match)
;	  ("<up>"    . ignore             )
;	  ("<down>"  . ignore             ))))

;(add-hook 'iswitchb-define-mode-map-hook 'iswitchb-local-keys)


;---------------------------------------------------------------------
; org-mode
;---------------------------------------------------------------------

;; The following lines are always needed.  Choose your own keys.
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cb" 'org-iswitchb)
(add-hook 'org-mode-hook 'turn-on-font-lock) 

;;parent can be DONE once all children are DONE
(setq org-enforce-todo-dependencies t)

;; once done, timestamp
(setq org-log-done 'time)

;;remember
(org-remember-insinuate)
(setq org-directory "~/Donnees/Org/")
(setq org-default-notes-file (concat org-directory "/notes.org"))
(define-key global-map "\C-cr" 'org-remember)

;;agenda
(setq org-agenda-include-diary t)
