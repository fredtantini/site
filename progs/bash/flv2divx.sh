#!/bin/bash

if [ ! $2 ]
then 
 sortie=${1%%.*}.avi
else
 sortie=$2
fi


if [ ${1##*.} == flv ]
then
    mencoder $1 -ovc lavc -lavcopts vcodec=mpeg4:vbitrate=768:mbd=2:v4mv:autoaspect -vf pp=lb -lameopts fast:preset=standard -oac mp3lame -o $sortie
else
    echo le premier est le .flv, le second le .avi
fi
