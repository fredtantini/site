if [ ! $2 ]
then 
 sortie=$1.flv
else
 sortie=$2
fi

if [ ${1##*.} == ASF ]
then
 mencoder -of lavf -oac mp3lame -lameopts abr:br=56 -ovc lavc -lavcopts vcodec=flv:vbitrate=400:mbd=2:mv0:trell:v4mv:cbp:last_pred=3 -of lavf -lavfopts format=flv -srate 22050 -ofps 24000/1001 -af volume=30 -vf eq=20:0 -o $sortie $1
else
 echo le premier est le .asf, le second le .flv
fi
