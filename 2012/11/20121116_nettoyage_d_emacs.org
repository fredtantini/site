#+DATE: <2012-11-16>
#+TITLE: Le nettoyage de mon .emacs 

* Pourquoi
  Parce que. Un peu plus de 900 lignes, ça commence à faire pas mal. Surtout quand plein de trucs sont plus utilisés (entre autre suite à divers changements de PC). J'ai essayé de faire ça pas trop mal : dans des dossiers tout bien rangé. De plus, tout ce qui peut être =customised= sera fait dans =Customize=.

Je vous conseille de regarder le manuel d'emacs sur [[http://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html][le fichier d'initialisation]] et le petit résumé de ce qu'il se passe au [[http://www.gnu.org/software/emacs/manual/html_node/elisp/Startup-Summary.html][démarrage]], c'est toujours instructif.
* DONE Première étape : les dossiers
  CLOSED: [2012-11-17 sam. 17:53]
  Même si je suis supporter de [[http://standards.freedesktop.org/basedir-spec/basedir-spec-latest.html][$XDG_CONFIG_HOME]] (toutes les configs vont dans le dossier =.config=), je vais laisser le fichier =.emacs= dans mon /home/, ainsi que le dossier =.emacs.d= (je sais, je pourrais enlever =.emacs= pour travailler sur =.emacs/init.el= mais sentimentalement, je suis pas prêt).

  Concernant le backup et l'auto-save, je laisse ça par défaut, sachant que ça peut se régler (cf [[http://www.gnu.org/software/emacs/manual/html_node/tramp/Auto_002dsave-and-Backup.html][le manuel]], et les variables =backup-directory-alist= et =auto-save-file-name-transforms=). Avoir ces fichiers dans le dossier du fichier édité me convient parfaitement.

  Le reste de la configuration est dans =.emacs.d/config/= (=.place=, =.org-timestamps=…) et =.emacs.d/site-lisp/= (=key-bindings.el=, =mode-mappings.el=… et surtout =custom.el=).

* DONE Deuxième étape : customization 
  CLOSED: [2012-11-17 sam. 17:53]

  J'ai commencé à me servir de la customization en essayant de configurer =org-mode=, et ça m'a permis de (re)découvrir cette fonctionnalité d'emacs (ô combien disgracieuse à mes yeux). Malgré son aspect qui ne me ragoûte pas, j'ai pu découvrir pas mal d'options — dont je parlerai peut-être à l'occasion — en naviguant à travers les groupes. Je me suis alors mis en tête de définir tout ce qui était customizable par customize. 

** Je la mets dans un fichier
   :PROPERTIES:
   :ID:       ff70add5-2a0c-41e7-aef8-057bc9150368
   :Created:  [2013-02-03 dim. 15:19]
   :END:

   Comme je veux un .emacs relativement court, je ne veux pas un custom-set-variables en plein milieu qui fasse 500 lignes. Pour ça, on regarde le [[http://www.gnu.org/software/emacs/manual/html_node/emacs/Saving-Customizations.html][manuel d'emacs]] qui nous dit comment faire :
#+begin_src elisp
(setq custom-file "~/.emacs-custom.el")
(load custom-file)
#+end_src

** Je customize tout ce que je peux customizer
   :PROPERTIES:
   :ID:       3bb80137-84fd-4113-9342-7a782c59d47a
   :Created:  [2013-02-03 dim. 15:19]
   :END:
   
   Pour ça, pour « chaque » ligne de mon .emacs, je me place sur la variable concernée et =[C-h v]=. Si je me suis effectivement placé sur le nom d'une variable, son nom est proposé par défaut. Dans le buffer d'aide, je regarde à la fin si la ligne « You can customize this variable » est présente. Si oui, ben je clique dessus. Je regarde alors sa valeur par défaut. Si c'est la même que celle de mon .emacs, j'enlève la customization. J'ajoute un commentaire pour savoir ce que fait la variable si nécessaire, et je sauve pour les sessions futures. Ces trois dernières opérations étant effectuées en cliquant sur le bouton =State=. J'en profite aussi généralement pour cliquer sur le =Group= auquel il appartient, pour voir d'autres options.



** Je supprime l'ancienne configuration de mon .emacs
   :PROPERTIES:
   :ID:       29d42795-4db5-4bf5-a4a1-cdcee0a7bb88
   :Created:  [2013-02-03 dim. 15:19]
   :END:
En effet, le =C-h v= de custom-file nous dit :
#+begin_quote
 It will not delete any customizations from the old custom file. You should do that manually if that is what you want.
#+end_quote 
* TODO Troisième étape : le fichier .emacs

  On commence par dire où sont les dossiers/config. Comme je travaille parfois sous windows, je ne veux donc pas que mon /home/ soit en dur.[fn:1]
#+begin_src elisp
(defvar home-dir (concat (getenv "HOME") "/"))
#+end_src

  Alternative si l'extension avec ~ marche :
#+begin_src elisp
(defvar home-dir)
(setq home-dir (concat (expand-file-name "~") "/"))
#+end_src

  J'en profite pour mettre dans [[http://www.gnu.org/software/emacs/manual/html_node/elisp/Library-Search.html][load-path]] mon =.emacs.d= où l'essentiel va se passer.
#+begin_src elisp
(defvar home-lisp-dir (list (concat home-dir ".emacs.d/")))
(setq load-path (append home-lisp-dir load-path))
#+end_src

  Au cas où je souhaiterai ajouter plusieurs dossiers, je me mets ça sous le coude :
#+begin_src elisp
;; Set up load path 
(setq load-path (append (list (concat home-dir ".emacs.d/")
			      (concat home-dir ".config/emacs/")
			      (concat home-dir ".config/site-lisp/"))
                        load-path))
#+end_src

  Dans ce dernier, un petit =subdirs.el= pour ajouter ses sous-dossiers.

  On charge ensuite la susdite customization.

#+begin_src elisp
(setq custom-file (concat home-lisp-dir "custom.el"))
(load custom-file)
#+end_src

Et on charge le reste
#+begin_src elisp
(load-library "mode-mapping")
(load-library "key-bindings")
(load-library "my-functions")

#+end_src
* TODO Quatrième étape : le reste
** TODO mode-mapping
   :PROPERTIES:
   :ID:       1efcb54f-8c4b-40d5-a61a-42e81bf80127
   :Created:  [2013-02-03 dim. 15:19]
   :END: 
On regarde =auto-mode-alist= pour connaitre quelles sont les extensions qui sont gérées. On regarde également =loaddefs.el= pour ce qui est déjà autoloadé. On ajoute ce qu'il faut. 

* Footnotes

[fn:1] pas bien sûr que ce soit utile… En plus risque de pourrir le fichier custom ?



